<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = false;

    /**
     * The table which linked with the model
     *
     * @var string
     */
    protected $table = 'favourite_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'link', 'author_name', 'creation_date'
    ];
}
