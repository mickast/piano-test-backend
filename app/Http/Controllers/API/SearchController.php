<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Query;

class SearchController extends Controller
{

    const SITE = 'stackoverflow';
    const ENDPOINT = "http://api.stackexchange.com/2.2/search";
    const FILTER = '!frpDrIm2*WME(RJpkmnQGwPl8KZb)96L6WG';

    /**
     * Searches for questions on the StackOverflow site
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function search(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'query' => 'required|max:25|min:2',
        ]);

        if ($validator->fails()) {

            $errors = [];
            foreach ($validator->errors()->toArray() as $errorBlock){

                $eCount = count($errorBlock);
                for($i = 0; $i < $eCount; ++$i){
                    $errors[] = $errorBlock[$i];
                }
            }

            $res = [
                'resType' => 1,
                'resErrors' => $errors,
                'resData' => []
            ];

            return response()->json($res, 200);
        }

        $queryString = $request->get('query');
        $page = ($request->get('page')) ? $request->get('page') : 1;
        $perPage = ($request->get('perPage')) ? $request->get('perPage') : 10;
        $sort = ($request->get('sort')) ? $request->get('sort') : 'activity';
        $order = ($request->get('order')) ? $request->get('order') : 'desc';

        $client = new \GuzzleHttp\Client();
        $response = $client->get(self::ENDPOINT, [
            'query' => [
                'page' => $page,
                'pagesize' => $perPage,
                'sort' => $sort,
                'intitle' => $queryString,
                'site' => self::SITE,
                'order' => $order,
                'filter' => self::FILTER
            ]
        ]);

        $stackOverflowData = json_decode($response->getBody());

        $favouriteList = \App\Question::where('user_id', Auth::user()->getAuthIdentifier())->get();

        $questionList = [];
        if (!empty($stackOverflowData->items)) {
            foreach ($stackOverflowData->items as $item) {

                $isFavourite = false;
                $fCount = count($favouriteList);
                for ($i = 0; $i < $fCount; ++$i) {
                    if($favouriteList[$i]->internal_id == $item->question_id){
                        $isFavourite = true;
                    }
                }

                $questionList[] = [
                    'internal_id' => $item->question_id,
                    'tags' => isset($item->tags) ? $item->tags : [],
                    'isAnswered' => $item->is_answered,
                    'viewCount' => $item->view_count,
                    'link' => $item->link,
                    'title' => htmlspecialchars($item->title),
                    'authorName' => $item->owner->display_name,
                    'authorAvatar' => isset($item->owner->profile_image) ? $item->owner->profile_image : null,
                    'score' => $item->score,
                    'creationDate' => $item->creation_date,
                    'isFavourite' => $isFavourite
                ];
            }
        }
        if($page == 1){
            $this->saveQuery($queryString);
        }

        $res = [
          'resType' => 0,
          'resErrors' => [],
          'resData' => [
              'questions' => $questionList,
              'total' => $stackOverflowData->total
          ]
        ];

        return response()->json($res, 200);
    }

    /**
     * Saves query data
     *
     * @param $queryString
     */
    public function saveQuery($queryString)
    {

        $query = new Query;

        $query->query = $queryString;
        $query->user_id = Auth::user()->getAuthIdentifier();

        $query->save();

    }
}
