<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class QueryController extends Controller
{

    /**
     * Shows query history
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQueryList()
    {

        $queryList = DB::table('queries')
            ->select(['query', 'name as user_name', 'queries.created_at'])
            ->leftJoin('users', 'users.id', '=', 'queries.user_id')
            ->get();

        $res = [
          'resType' => 0,
          'resErrors' => [],
          'resData' => ['queries' => $queryList]
        ];

        return response()->json($res,200);
    }
}
