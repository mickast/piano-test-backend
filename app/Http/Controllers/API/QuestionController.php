<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Question;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    /**
     * Shows favourite user's questions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavouriteQuestions()
    {

        $questionList = Question::where(
            'user_id', Auth::user()->getAuthIdentifier()
        )->select('id', 'title', 'link', 'author_name', 'creation_date', 'internal_id')->get();

        $res = [
            'resType' => 0,
            'resErrors' => [],
            'resData' => ['questions' => $questionList]
        ];

        return response()->json($res,200);
    }

    /**
     * Saves question like a favourite
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $question = new Question;

        $question->internal_id = $request->get('internal_id');
        $question->title = $request->get('title');
        $question->link = $request->get('link');
        $question->author_name = $request->get('author_name');
        $question->creation_date = date ("Y-m-d H:i:s", $request->get('creation_date'));
        $question->user_id = Auth::user()->getAuthIdentifier();

        if($question->save()){
            $res = [
                'resType' => 0,
                'resErrors' => [],
                'resData' => []
            ];
        } else{
            $res = [
                'resType' => 1,
                'resErrors' => ['Saving error'],
                'resData' => []
            ];
        }

        return response()->json($res,200);
    }

    /**
     * Deletes question from favourite
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromFavourite(Request $request){

        $question = Question::where('internal_id', $request->get('internal_id'));

        if($question->delete()){
            $res = [
                'resType' => 0,
                'resErrors' => [],
                'resData' => []
            ];
        } else{
            $res = [
                'resType' => 1,
                'resErrors' => ['Deleting error'],
                'resData' => []
            ];
        }
        return response()->json($res,200);
    }
}
