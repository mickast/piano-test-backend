<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        if (Auth::attempt(['login' => $request->get('login'), 'password' => $request->get('password')])) {
            $user = Auth::user();
            $res = [
                'resType' => 0,
                'resErrors' => [],
                'resData' => ['token' => $user->createToken('MyApp')->accessToken, 'userName' => $user->name]
            ];
            return response()->json($res, 200);
        } else {
            $res = [
                'resType' => 1,
                'resErrors' => ['Unauthorised'],
                'resData' => []
            ];
            return response()->json($res, 200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|',
            'login' => 'required|unique:users|max:10|min:4',
            'email' => 'email',
            'password' => 'required|min:6|max:15',
            'r_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {

            $errors = [];

            foreach ($validator->errors()->toArray() as $errorBlock){
                $eCount = count($errorBlock);
                for($i = 0; $i < $eCount; ++$i){
                    $errors[] = $errorBlock[$i];
                }
            }

            $res = [
                'resType' => 1,
                'resErrors' => $errors,
                'resData' => []
            ];
            return response()->json($res, 200);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $res = [
            'resType' => 0,
            'resErrors' => [],
            'resData' => ['token' => $user->createToken('MyApp')->accessToken, 'userName' => $user->name]
        ];

        return response()->json($res, 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $accessToken = Auth::user()->token();

        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        if($accessToken->revoke()){
            $res = [
                'resType' => 0,
                'resErrors' => [],
                'resData' => []
            ];
            return response()->json($res, 204);

        } else {
            $res = [
                'resType' => 1,
                'resErrors' => ['Logout error'],
                'resData' => []
            ];
            return response()->json($res, 200);
        }
    }
}
