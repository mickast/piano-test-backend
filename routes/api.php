<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('getQueryHistory', 'API\QueryController@getQueryList');

Route::post('login', 'API\AuthController@login')->name('login');

Route::post('register', 'API\AuthController@register')->name('register');

Route::group(['middleware' => 'auth:api'], function(){

    Route::get('search', 'API\SearchController@search')->name('search');
    Route::post('saveFavourite', 'API\QuestionController@save');
    Route::get('getFavourite', 'API\QuestionController@getFavouriteQuestions');
    Route::post('removeFavourite', 'API\QuestionController@removeFromFavourite');
    Route::post('logout', 'API\AuthController@logout');

});